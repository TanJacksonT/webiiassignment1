//function that will change the text color of the table
function changeTextColor() { 
   let textcolorValue = document.getElementById("text-color").value;
   table.style.color = textcolorValue;
}

//function that will change the background color of the table
function changeBackgroundColor() {
   let backgroundcolorValue= document.getElementById("background-color").value;
   table.style.backgroundColor = backgroundcolorValue;
}

//function that will change the border width of the table
function changeBorderWidth() { 
    
    let borderWidthValue = document.getElementById("border-width").value;
    let td = document.getElementsByTagName("td");
    for (let i = 0 ; i < td.length; i++) {
    td[i].style.borderWidth = borderWidthValue + "px";
    }
}

//function that will change the border color of the table
function changeBorderColor() {
    let td = document.getElementsByTagName("td");
    let borderColorValue = document.getElementById("border-color").value;
    for (let i = 0; i < td.length; i++ ) {
    td[i].style.borderColor = borderColorValue;  
    }
}

//function that will change the table width of the table
function changeTableWidth() {
    let tableWidthValue = document.getElementById("table-width").value;
    table.style.width = tableWidthValue + "%";
}

