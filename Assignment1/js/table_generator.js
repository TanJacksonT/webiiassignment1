

function generateTable() {

    /* function that generates a table depending on the user input for the row and column
       Also, it will show the html of the table updated.
    */

    let colcount = document.getElementById("col-count").value;
    let rowcount = document.getElementById("row-count").value;
    let tableStartId = "<table id = table>";
    let tableStartIdHTML = "&lt;table id = table&gt;" + "<br>";
    let tableBody = "";
    let tableBodyHTML = "";
    

    for( var i = 0; i < rowcount; i++){
        tableBody += "<tr>";
        tableBodyHTML += "&lt;tr&gt;" + "<br>";
        for(var j=0; j<colcount; j++){
            tableBody += "<td>";
            tableBody += "cell" + i + j;
            tableBody += "</td>";
            tableBodyHTML += "&lt;td&gt;";
            tableBodyHTML += "&lt;cell" + i + j + "&gt;";
            tableBodyHTML += "&lt;/td&gt; ";
        }   
            tableBody += "</tr>";
            tableBodyHTML += "<br>" + "&lt;/tr&gt;" + "<br>";
    }
  


 

    let tableEndId = "</table>";
    let tableEndIdHTML = "&lt;/table&gt;";
    document.getElementById("table-render-space").innerHTML = tableStartId + tableBody + tableEndId;
    document.getElementById("table-html-space-background").innerHTML = tableStartIdHTML + tableBodyHTML + tableEndIdHTML;
    let backgroundcolorValue= document.getElementById("background-color").value;
    table.style.backgroundColor = backgroundcolorValue;
}



//functions that will keep running whenever updates are made in the table.
function contentLoaded() {

    let nums_column  = document.getElementById("col-count");
    let nums_row = document.getElementById("row-count");  
    nums_column.addEventListener("input",generateTable);
    nums_row.addEventListener("input",generateTable);  

    let textcolor = document.getElementById("text-color");
    textcolor.addEventListener("input",changeTextColor);
 
    let backgroundcolor = document.getElementById("background-color");
    backgroundcolor.addEventListener("input",changeBackgroundColor);
 
    let borderWidth = document.getElementById("border-width");
    borderWidth.addEventListener("input",changeBorderWidth);

    let borderColor = document.getElementById("border-color");
    borderColor.addEventListener("input",changeBorderColor);
 
    let tableWidth = document.getElementById("table-width");
    tableWidth.addEventListener("input",changeTableWidth);

    generateTable();

}

 document.addEventListener("DOMContentLoaded",contentLoaded);